# history {{{
  HISTSIZE=10000
  SAVEHIST=9000
  HISTFILE=$HOME/.zsh_history
# }}}

# useful plugins {{{
  plugins=(
    extract
    vi-mode
    autojump
    zsh-autosuggestions
    zsh-syntax-highlighting
    history-substring-search
  )
# }}}

# z-shell persistent, global settings {{{
  ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets pattern cursor)
  ZSH_THEME=powerlevel10k/powerlevel10k
  ZSH_DISABLE_COMPFIX=true
# }}}

# exports {{{
  export ZSH=$HOME/.oh-my-zsh
  export PATH="/usr/local/texlive/2020/bin/x86_64-darwin:$PATH"
  export PATH="/usr/local/opt/gnu-sed/libexec/gnubin:$PATH"
  source $HOME/.cargo/env
  source $ZSH/oh-my-zsh.sh
# }}}

# arc colorscheme: adapted from https://github.com/kaicataldo/material.vim {{{
  alias arc='
  cp $HOME/MEGAsync/Dotfiles/kitty/kitty-arc.conf $HOME/.config/kitty/kitty.conf
  cp $HOME/MEGAsync/Dotfiles/alacritty/alacritty-arc.yml $HOME/.config/alacritty/alacritty.yml
  cp $HOME/MEGAsync/Dotfiles/vim/.vimrc.arc $HOME
  cp $HOME/MEGAsync/Dotfiles/tmux/.tmux.conf.arc $HOME
  find $HOME -maxdepth 1 -name "*.arc" | while read file; do mv $file ${file%.*}; done
  tmux source-file $HOME/.tmux.conf'
# }}}

# nord colorscheme: adapted from https://www.nordtheme.com {{{
  alias nord='
  cp $HOME/MEGAsync/Dotfiles/kitty/kitty-nord.conf $HOME/.config/kitty/kitty.conf
  cp $HOME/MEGAsync/Dotfiles/alacritty/alacritty-nord.yml $HOME/.config/alacritty/alacritty.yml
  cp $HOME/MEGAsync/Dotfiles/vim/.vimrc.nord $HOME
  cp $HOME/MEGAsync/Dotfiles/tmux/.tmux.conf.nord $HOME
  find $HOME -maxdepth 1 -name "*.nord" | while read file; do mv $file ${file%.*}; done
  tmux source-file $HOME/.tmux.conf'
# }}}

# light colorscheme: adapted from https://github.com/NLKNguyen/papercolor-theme {{{
  alias light='
  cp $HOME/MEGAsync/Dotfiles/kitty/kitty-light.conf $HOME/.config/kitty/kitty.conf
  cp $HOME/MEGAsync/Dotfiles/alacritty/alacritty-light.yml $HOME/.config/alacritty/alacritty.yml
  cp $HOME/MEGAsync/Dotfiles/vim/.vimrc.light $HOME
  cp $HOME/MEGAsync/Dotfiles/tmux/.tmux.conf.light $HOME
  find $HOME -maxdepth 1 -name "*.light" | while read file; do mv $file ${file%.*}; done
  tmux source-file $HOME/.tmux.conf'
# }}}

# solarized colorscheme: adapted from https://github.com/NLKNguyen/papercolor-theme {{{
  alias sol='
  cp $HOME/MEGAsync/Dotfiles/kitty/kitty-sol.conf $HOME/.config/kitty/kitty.conf
  cp $HOME/MEGAsync/Dotfiles/alacritty/alacritty-sol.yml $HOME/.config/alacritty/alacritty.yml
  cp $HOME/MEGAsync/Dotfiles/vim/.vimrc.sol $HOME
  cp $HOME/MEGAsync/Dotfiles/tmux/.tmux.conf.sol $HOME
  find $HOME -maxdepth 1 -name "*.sol" | while read file; do mv $file ${file%.*}; done
  tmux source-file $HOME/.tmux.conf'
# }}}

# one colorscheme: adapted from https://github.com/rakr/vim-one {{{
  alias one='
  cp $HOME/MEGAsync/Dotfiles/kitty/kitty-one.conf $HOME/.config/kitty/kitty.conf
  cp $HOME/MEGAsync/Dotfiles/alacritty/alacritty-one.yml $HOME/.config/alacritty/alacritty.yml
  cp $HOME/MEGAsync/Dotfiles/vim/.vimrc.one $HOME
  cp $HOME/MEGAsync/Dotfiles/tmux/.tmux.conf.one $HOME
  find $HOME -maxdepth 1 -name "*.one" | while read file; do mv $file ${file%.*}; done
  tmux source-file $HOME/.tmux.conf'
# }}}

# dark colorscheme: adapted from https://github.com/bluz71/vim-moonfly-colors {{{
  alias dark='
  cp $HOME/MEGAsync/Dotfiles/kitty/kitty-dark.conf $HOME/.config/kitty/kitty.conf
  cp $HOME/MEGAsync/Dotfiles/alacritty/alacritty-dark.yml $HOME/.config/alacritty/alacritty.yml
  cp $HOME/MEGAsync/Dotfiles/vim/.vimrc.dark $HOME
  cp $HOME/MEGAsync/Dotfiles/tmux/.tmux.conf.dark $HOME
  find $HOME -maxdepth 1 -name "*.dark" | while read file; do mv $file ${file%.*}; done
  tmux source-file $HOME/.tmux.conf'
# }}}

# bat theme and configuration file {{{
  export BAT_CONFIG_PATH="$HOME/.config/bat/bat.conf"
  export BAT_THEME=$COLORSCHEME
# }}}

# fzf default configuration settings {{{
  export FZF_DEFAULT_OPTS='
    --prompt="> "
    --marker="> "
    --pointer=">"
    --height=100%
    --layout=reverse
    --bind=ctrl-w:up,ctrl-s:down
    --color=fg:-1,bg:-1,hl:02,fg+:-1,bg+:-1,hl+:05,info:04,prompt:02,pointer:05,marker:03,spinner:01,border:15
    --preview "bat --theme=$COLORSCHEME --color=always {}"
  '
# }}}

# lsd {{{
  alias l='lsd --oneline --group-dirs first --size short --icon never'
  alias ls='lsd --group-dirs first --size short --icon never'
  alias ll='lsd --long --group-dirs first --size short --blocks permission,user,size,date,name --icon never'
  alias la='lsd --long --all --group-dirs first --size short --blocks permission,user,size,date,name --icon never'
  export LS_COLORS='fi=32:pi=33:cd=33:di=34;04:so=31:ln=35:ex=33:bd=33:or=31'
# }}}

# editor of choice vim {{{
  alias v='/usr/local/Cellar/vim/8.2.0850/bin/vim'
# }}}

# internet {{{
  alias ytv='youtube-dl -ic'
  alias yta='youtube-dl -icx --audio-format mp3'
# }}}

# version control {{{
  alias gi="git init"
  alias gs="git status"
  alias gd="git diff"
  alias gdh="git diff HEAD"
  alias gc="git clone $1 $2"
  alias gcm="git commit -m "$1""
  alias gaa="git add -A ."
  alias gpo="git push origin $1"
  alias gl="git log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit"
  alias glo="git log --oneline"
  alias grhh="git reset --hard HEAD"
  alias gcp="git cherry-pick $1"
# }}}

# npm {{{
  alias ni='npm install'
  alias nis='npm i -S'
  alias nid='npm i -D'
  alias nig='npm i -g'
  alias nr='npm run $1'
  alias nrs='npm run start'
  alias nrb='npm run build'
  alias nrt='npm run test'
  alias nrc='npm run commit'
# }}}

# system aliases {{{
  alias sys='archey -o'
  alias pipes='pipes.sh'
# }}}

# conda initializer {{{
  __conda_setup="$('/opt/miniconda3/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
  if [ -f "/opt/miniconda3/etc/profile.d/conda.sh" ]; then
    . "/opt/miniconda3/etc/profile.d/conda.sh"
  else
    export PATH="/opt/miniconda3/bin:$PATH"
  fi
  unset __conda_setup
# }}}

# terminal prompt and fzf tab completions {{{
  [[ ! -f $HOME/.p10k.zsh ]] || source $HOME/.p10k.zsh
  [[ ! -f $HOME/.fzf.zsh  ]] || source $HOME/.fzf.zsh
# }}}
