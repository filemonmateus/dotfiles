# :rocket: .files

A selective, inescapably useful collection and snapshot containing individual dot files and related configuration artifacts for mutt, vim, zsh, tmux, and friends.

## Preview

<img src="preview.png"/>
